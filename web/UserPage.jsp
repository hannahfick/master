<%--
  Created by IntelliJ IDEA.
  User: hf57
  Date: 17/01/2019
  Time: 2:21 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
    <head>
        <meta charset="UTF-8">
        <title>User Profile</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
              integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ"
              crossorigin="anonymous">


    </head>
    <body>
        <div class="container-fluid">
            <br>
            <br>
            <%--ADDING FORM FOR UPLOADING USER PIC--%>
            <h4>Upload a pic:</h4>
            <form action="UploadServlet" method="post" enctype="multipart/form-data">
                <input type="file" name="userpic" size="50"/>
                <input type="submit" value="Upload Pic">
            </form>

            <form>
                <div class=col-sm-4 style="border: black">
                    <img src="${user.url}" width="225">
                    <br>

                    <label for="name">Your Name</label>
                    <br>
                    <core:set var="name" value="${user.name}"/>
                    <input type="text" id="name" placeholder="${user.name}">
                    <br>
                    <label for="from">From</label>
                    <br>
                    <input type="text" id="from" placeholder="Hamilton">
                    <br>
                    <hr>
                    <input type="submit" id="explore"
                           value="Explore!">
                    <hr>
                </div>


                <p> Don't forget your Login Code! _______
                    <input type="submit" id="logout"
                           value="Logout!"></p>
                <hr>
                <br>
                <div class="col-sm-8">
                    <input type="text" id="submission" size="50" placeholder="What are you grateful for?">
                    <br>
                    <input type="submit" id="save"
                           value="Save this Memory!!">
                    <br>
                    <br>
                    <input type="text" id="lastPost" size="50" placeholder="_____ you were grateful for: ">
                    <br>
                    <br>
                    <input type="text" id="otherPost" size="50" placeholder="On _____ you were grateful for: ">
                    <br>
                    <br>
                    <input type="submit" id="memories"
                           value="Show me more memories!">

                </div>
            </form>

        </div>
    </body>
</html>