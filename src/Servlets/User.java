package Servlets;

import java.io.Serializable;
import java.util.List;

public class User implements Serializable {
    /* todo complete the user object with appropriate instance variables, constructors and getters/setters.
    variables that may be needed:
     login number, name, location, list of memories/gratitudes, picture url.
    * */
    private int id;
    private String name,location,url;
    private List<String> cornys;

    public List<String> getCornys() {
        return cornys;
    }

    public void setCornys(List<String> cornys) {
        this.cornys = cornys;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public User(int id, String name, String location, String url) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.url = url;
    }






}
