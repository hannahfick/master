package Servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


public class Login extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        //todo add function to take a number, pass to a dao to retrieve the user info. dao should return success or failure, success is a user object, failure should trigger sending back to index/login page with a message that no user exists.
        //will likely need a session to check login status. not used yet.
        HttpSession session = request.getSession(); // this will create a session if one doesn't exist.

        //connect to db and retriev user data
        UserDAO dbConection = new UserDAO(getServletContext());
        User user = dbConection.getUser(request.getParameter("login"));

        //debugging printwriter
        PrintWriter out = response.getWriter();

        //check if returned user is null ie user not found. if so send back to homepage. home page to look if user attribute exists. if so display user not found error.
        if (user == null) {
            session.setAttribute("user",user);
            request.setAttribute("user", user);
            request.getRequestDispatcher("HomePage.jsp").forward(request, response);
        } else {
            session.setAttribute("user",user);
            request.setAttribute("user", user);
            request.getRequestDispatcher("UserPage.jsp").forward(request, response);
        }

    }


    //not used, will deal with requests via post to ensure confidentiality of log in numbers
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }
}
