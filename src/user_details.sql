DROP TABLE IF EXISTS user_details;
CREATE TABLE IF NOT EXISTS user_details (
  user_id INT(6) zerofill NOT NULL AUTO_INCREMENT,
  name VARCHAR(255),
  location VARCHAR(255),
  img_src VARCHAR(255),
  PRIMARY KEY (user_id)
)AUTO_INCREMENT=100000;

INSERT INTO user_details (name,location,img_src)
VALUES
('Jane Bloggs','Hamilton','Photos/batman_silhouette_by_icedragon529.jpg'),
('Name With Space','Placethatdoesntexist','Photos/batman_silhouette_by_icedragon529.jpg'),
('username1','location1','Photos/batman_silhouette_by_icedragon529.jpg');


# test query
# SELECT * FROM user_details WHERE user_id =1;
# UPDATE user_details SET img_src ='Photos/Placeholder_Image.jpg' WHERE user_id = 100001;
SELECT * FROM user_details;