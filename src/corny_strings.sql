DROP TABLE IF EXISTS corny_strings;
CREATE TABLE IF NOT EXISTS corny_strings (
  user_id INT NOT NULL,
  string_id INT auto_increment,
  input  VARCHAR(80),
  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (string_id)
);

INSERT INTO corny_strings(user_id ,input)
VALUES ('1','Lorem ipsum dolor sit amet, consem vel'),
       ('1','a mattis risus tristique siodo vitae ac lacus.' ),
       ('1','Mauris hendrerit ipsum non lectus porttitor commodo' ),
       ('1','Mauris nec odio vitae purus varius aliquet.' ),
       ('1','Cras eu odio quis mi molestie ullamcorper' );